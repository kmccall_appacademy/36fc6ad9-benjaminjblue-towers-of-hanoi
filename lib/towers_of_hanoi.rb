# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers, :move_count
  def initialize
    @towers = [[5,4,3,2,1],[],[]]
    @move_count = 0
  end

  def play
    game_over = false
    while !game_over
      render(@towers)
      potential_move = get_move
      if valid_move?(@towers, potential_move[0], potential_move[1])
        @towers = move(@towers, potential_move[0], potential_move[1])
      else
        puts "invalid move, try again"
      end
      game_over = won?(@towers)
      @move_count += 1
    end
    render(@towers)
    puts "Congrats, you won in #{move_count} moves!"
  end

  def render(towers)
    i = 4
    puts
    while i >= 0
      #towers display a space for each position without a disc
      #and underscore in the base position if no discs
      #otherwise each position is filled with cooresponding numbers from 'towers' array
      towers_display = (0..2).map{|j| towers[j][i] || (i == 0 ? "_" : " ")}
      line = "#{towers_display[0]} #{towers_display[1]} #{towers_display[2]}"
      puts line if line != " " * 5
      i -= 1
    end
    puts
  end

  def won?(towers)
    towers[1] == [5,4,3,2,1] || towers[2] == [5,4,3,2,1]
  end

  def valid_move?(towers, from_tower, to_tower)
    #take from a tower with a disc
    towers[from_tower].length > 0 &&
    ( #move to a tower with no discs or with a bigger disc
      towers[to_tower].length == 0 ||
      towers[to_tower].last > towers[from_tower].last
    )
  end

  def get_move
    puts "move from tower [your input on line below]:"
    from = gets.chomp.to_i
    puts "move to tower [your input on line below]:"
    to = gets.chomp.to_i
    return [from, to]
  end

  def move(towers, from, to)
    #copy tower to keep mutations in 'play' method
    new_towers = towers.slice(0..2)
    #pop disc from old tower and push to new tower
    new_towers[to].push(new_towers[from].pop)
    new_towers
  end

end

game = TowersOfHanoi.new
game.play
